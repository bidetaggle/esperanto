<?php
$params = '
{
    "debug":                false,
    "httpsEnabled":         true,
    "databaseEnabled":      true,
'.
    //path informations
'   "rootURL":              "localhost/esperanto",
    "rootPath":             "/srv/http/esperanto",
    "indexModule":          "index",
'.
    //database
'   "databaseHost":         "localhost",
    "databasePort":         3306,
    "databaseName":         "esperanto",
    "databaseUser":         "root",
    "databasePassword":     "root",
    "databaseTablePrefix":  "esperanto_",
'.
    //root access : 3jk3V8qf
'   "passwordHash":         "$2y$12$mluhsGSLMQOC55L6djRgI.nNXz66shA0MHGXgGn81wiynv3jyvCDy",
    "passwordHashCost":     12,
'.
    //Misc
'   "salt":                 "",
'.
    //contact
'   "mailKWAM":             "nomail@nomail.com",
    "websiteName":          "Esperanto Manual / Manlibro / Manuel d\'Esperanto",
    "mailOwner":            "owner@mail.fr"
}
';
?>
