<?php

namespace Lib;

use PDO;

class Database
{
    /**
     * @var PDO
     */
    public static $dbh;

    public static function connect()
    {
        if(Conf::$databaseEnabled){
            self::$dbh = new PDO(
                Conf::getDSN(),
                Conf::$databaseUser,
                Conf::$databasePassword
            );

            self::$dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            self::$dbh->exec("SET NAMES 'UTF8'");
        }
    }
}
