<!DOCTYPE html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0, shrink-to-fit=yes">
    <title><?php echo Lib\Conf::$websiteName; ?></title>
    <link rel="icon" href="<?php echo Lib\Conf::$rootURL; ?>/Public/favicon.ico" type="image/x-icon">
    <?php foreach ($CtoV['CORE_header']['css'] as $fileName): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Lib\Conf::$rootURL . '/Public/' . $fileName; ?>" />
    <?php endforeach; ?>
    <?php foreach ($CtoV['CORE_header']['js'] as $fileName): ?>
        <script src="<?php echo Lib\Conf::$rootURL . '/Public/' . $fileName; ?>"></script>
    <?php endforeach; ?>
    <?php foreach ($CtoV['CORE_header']['modules'] as $moduleStyle): ?>
        <link rel="stylesheet" type="text/css" href="<?php echo Lib\Conf::$rootURL . '/' . $moduleStyle; ?>?Public=style.css" />
    <?php endforeach; ?>
</head>

<body>
<?php if(Lib\Conf::$debug == true): ?>
<pre>
Debug mode set to true in parameters.php:
<?php var_dump($_SESSION); ?>
<?php var_dump($CtoV); ?>
</pre>
<?php endif; ?>
