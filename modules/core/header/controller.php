<?php

use Lib\IO;
use Lib\Secure;
use Lib\Conf;
use Model\Header;

try
{
	/*
	scan the Public/style.css directories among the modules
	*/
	$CtoV['CORE_header'] = array(
		'modules' => array(),
		'css' => array(),
		'js' => array());
	$modules = scandir(Conf::$rootPath .'/modules');

	/*
	add all the style.css found in $module/Public
	In the future, this feature will be improved by scanning the Public folder of only the called module(s?)
	*/
	foreach ($modules as $mod) {
		if(	file_exists(Conf::$rootPath . '/' . '/modules/' . $mod . '/Public/style.css')
			&& !preg_match("/\.?\./", $mod))
			array_push($CtoV['CORE_header']['modules'], $mod);
	}

	/*
	scan /Public folder and add the files in <head> tag
	*/
	$globalPublicFolder = scandir(Conf::$rootPath . '/Public');
	foreach ($globalPublicFolder as $publicFile) {
		$ext = substr(strrchr($publicFile, '.'), 1);

		switch ($ext) {

			case 'css':
				array_push($CtoV['CORE_header']['css'], $publicFile);
				break;

			case 'js':
				array_push($CtoV['CORE_header']['js'], $publicFile);
				break;
		}
	}

	include_once("view.php");
}
catch (Exception $e)
{
	IO::displayException($e);
}

?>
