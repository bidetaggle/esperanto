<?php

use Lib\IO;
use Lib\Conf;

try
{
	include_once("view.php");
    include_once(Conf::$rootPath . "/modules/core/footer/controller.php");
}
catch (Exception $e)
{
	IO::displayException($e);
}

?>
