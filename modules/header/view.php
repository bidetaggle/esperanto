<?php use Lib\Conf; ?>

<header>
    <a href="<?php echo Conf::$rootURL; ?>">
        <img class="anim-hover-rotate" src="Public/logo.png" alt="Logo Esperanto" />
    </a>
    <a href="<?php echo Conf::$rootURL; ?>">
        <h1><span class="anim-hover-vibrate"><?php echo Conf::$websiteName; ?></span></h1>
    </a>
</header>
