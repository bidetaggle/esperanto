<?php

use Lib\IO;
use Lib\Conf;
use Model\Index;

try
{
	$CtoV = array();
	$index = new Index;

	$CtoV["vocabulary"] = $index->getAllVocabulary();

	include_once(Conf::$rootPath . "/modules/header/controller.php");
	include_once("view.php");
	include_once(Conf::$rootPath . "/modules/footer/controller.php");
}
catch (Exception $e)
{
	IO::displayException($e);
}

?>
