<?php
namespace Model;

use Lib\SQL;

class Index extends AbstractModel {

    public function getAllVocabulary(){
        $return = array();

        $catRequest = new SQL('category');
        foreach($catRequest->selectAll() as $category){
            $vocabRequest = new SQL('vocabulary');
            $vocabRequest->setWhere('id_category', $category["id"]);
            $return[$category['name']] = $vocabRequest->selectAll();
        }
        return $return;
	}
}
?>
