<main>
<?php foreach($CtoV["vocabulary"] as $category => $vocab): ?>
		<table>
			<thead>
				<tr>
					<th colspan="3"><?php echo $category; ?></th>
				</tr>
			</thead>
			<tr>
				<th>word</th>
				<th>mot</th>
				<th>vorto</th>
			</tr>
			<tbody>
				<?php foreach ($vocab as $translate): ?>
					<tr>
						<td class="english"><?php echo $translate["word"]; ?></td>
							<td class="esperanto">
								<a href="https://en.wiktionary.org/wiki/<?php echo $translate["vorto"]; ?>#Esperanto" target="_blank">
									<?php echo $translate["vorto"]; ?>
								</a>
							</td>
						<td><?php echo $translate["mot"]; ?></td>
					</tr>
				<?php endforeach; ?>
			</tbody>
		</table>
<?php endforeach; ?>
</main>
