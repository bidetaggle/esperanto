<?php
header( 'content-type: text/html; charset=utf-8; Access-Control-Allow-Origin: *' );
use Lib\Conf;
use Lib\Database;
use Lib\Cron;

////////////////////////////////////////////////
//				main includes 				  //
////////////////////////////////////////////////

include_once("libraries/configuration.lib.php");
include_once("libraries/io.lib.php");
include_once("libraries/database.lib.php");
include_once("libraries/sql.lib.php");
include_once("libraries/log.lib.php");
include_once("libraries/secure.lib.php");
include_once("libraries/mail.lib.php");
include_once("libraries/cron.lib.php");

include_once("libraries/abstract.model.php");

//Initialisation
Conf::initialize();
Database::connect();

//Ouverture de session
session_set_cookie_params(10 * 365 * 24 * 60 * 60);
session_start();


if(empty($_SESSION)){
	$_SESSION["login"] = NULL;
	$_SESSION["id"] = NULL;
	$_SESSION["lang"] = 'en';
}

if(isset($_GET['lang'])){
	switch ($_GET['lang']) {
		case 'fr':
			$_SESSION['lang'] = 'fr';
			break;

		default:
			$_SESSION['lang'] = 'en';
			break;

	}
	header('Location: ' . Conf::$rootURL . '/' . $_GET['file'] . '.php');
}

////////////////////////////////////////////////
//					router	 				  //
////////////////////////////////////////////////

// website root module name
if(empty($_GET['file']) || $_GET['file'] == "index.php")
	$_GET['file'] = Conf::$indexModule;
// modules
if(is_dir(Conf::$rootPath . "/modules/" . basename($_GET['file'])))
	$module = "modules/" . basename($_GET['file']);
// modules core
elseif(is_dir(Conf::$rootPath . "/modules/core/" . basename($_GET['file'])))
	$module = "modules/core/" . basename($_GET['file']);
// error 404
else{
	$module = "modules/core/error";
}

// Public folder for modules
if(isset($_GET['Public']) && !empty($_GET['Public']) && is_string($_GET['Public']))
{
	$publicFile = htmlspecialchars($_GET['Public']);
	if(!preg_match("/\.\./", $publicFile)){
		if(!file_exists($module . "/Public/" . $publicFile))
			echo "file " . $module . "/Public/" . $publicFile . " not found";
		else {
			$ext = substr(strrchr($publicFile, '.'), 1);

			switch ($ext) {
				case 'html':
					header("Content-type: text/html");
					break;

				case 'css':
					header("Content-type: text/css");
					break;

				case 'js':
					header('Content-type: application/javascript');
					break;

				case 'json':
					header("Content-type: application/json");
					break;
			}

			include_once($module . "/Public/" . $publicFile);
		}
	}
	else {
		echo "usage of /../ is forbidden";
	}
}
// Module call
else{
	if(file_exists($module . "/model.php"))
		include_once($module . "/model.php");

	// prompt api data
	if(isset($_GET['api']))
		include_once($module . "/api.php");
	// prompt regular Model View Controller
	else
		include_once($module . "/controller.php");
}

?>
